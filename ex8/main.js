Vue.component('message', {
	props: ['header', 'body'],
	template: `
		<div class="message" v-show="isVisible">
			<div class="message-header">
				{{ header }}
				<button @click="hideMessage">x</button>
			</div>
			<div class="message-body">
				{{ body }}
			</div>
		</div>
	`,
	data() {
		//Prvo napraviti bez ovoga icega isVisible, samo poruku izdvojiti ovako s parametrima, pa onda dodati ovo dugme za brisanje
		return {
			isVisible: true
		}
	},
	methods: {
		hideMessage() {
			this.isVisible = false;
		}
	}
});

var app = new Vue({
	el : '#root'
});
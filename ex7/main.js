Vue.component('task-list', {
	template: `
		<ul>
			<task v-for="task in tasks">{{ task.name }}</task>
		</ul>
	`,
	data() { //U KOMPONENTAMA, MORA BITI FUNKCIJA KOJA VRACA OBJEKAT S POLJIMA KOJA NAM TREBAJU
		return {
			tasks: [
				{ name: "Task 1", completed: true },
				{ name: "Task 2", completed: false },
				{ name: "Task 3", completed: true },
				{ name: "Task 4", completed: false },
				{ name: "Task 5", completed: false }
			]
		}
	}
});

Vue.component('task', {
	template: '<li><slot></slot></li>'
});

var app = new Vue({
	el : '#root'
});